# -*- mode: ruby -*-
# vi: set ft=ruby :

# Declaration of guest working directories
HOME_VAGRANT = "/home/vagrant"
HOME_CI = "#{HOME_VAGRANT}/PortableCI"
JENKINS_HOME_VOLUME = "#{HOME_CI}/volumes/jenkins/var/jenkins_home"
POSTGRES_DATA_VOLUME = "#{HOME_CI}/volumes/postgres/var/lib/postgresql/data"
SONAR_CONF_VOLUME = "#{HOME_CI}/volumes/sonar/opt/sonarqube/conf"
SONAR_DATA_VOLUME = "#{HOME_CI}/volumes/sonar/opt/sonarqube/data"
SONAR_EXT_VOLUME = "#{HOME_CI}/volumes/sonar/opt/sonarqube/extensions"
SONAR_LIB_VOLUME = "#{HOME_CI}/volumes/sonar/opt/sonarqube/lib/bundled-plugins"

$initInstallScript = <<SCRIPT
# Installing docker from doc: https://docs.docker.com/engine/installation/linux/ubuntu/
sudo apt-get update
sudo apt-get install \
linux-image-extra-$(uname -r) \
linux-image-extra-virtual
sudo apt-get install \
apt-transport-https \
ca-certificates \
curl \
software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y install docker-ce="17.03.1~ce-0~ubuntu-trusty"
sudo apt-cache madison docker-ce
docker --version
# Installing docker compose from doc: https://docs.docker.com/compose/install/
# Docker compose releases: https://github.com/docker/compose/releases
curl -L https://github.com/docker/compose/releases/download/1.14.0-rc2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
SCRIPT

$initDirTreeScript = <<SCRIPT
# Create CI folder into guest
sudo mkdir -p #{HOME_CI}
# Init volumes dirs
sudo mkdir -p #{JENKINS_HOME_VOLUME}
sudo mkdir -p #{POSTGRES_DATA_VOLUME}
sudo mkdir -p #{SONAR_CONF_VOLUME}
sudo mkdir -p #{SONAR_DATA_VOLUME}
sudo mkdir -p #{SONAR_EXT_VOLUME}
sudo mkdir -p #{SONAR_LIB_VOLUME}
# Set vagrant user access
sudo chown -R 1000:1000 #{HOME_VAGRANT}
SCRIPT


$launchScript = <<SCRIPT
# Launch docker env
cd #{HOME_CI}
sudo docker-compose up -d
sudo docker-compose ps
SCRIPT

# Version(s) required of Vagrant
Vagrant.require_version ">= 1.9.5"

# VM configuration
Vagrant.configure("2") do |config|
# Time in seconds that Vagrant will wait to start the machine and be accessible (300 by default)
  config.vm.boot_timeout = 180

# Time in seconds that Vagrant will wait for the machine to stop properly (60 by default)
  config.vm.graceful_halt_timeout = 120

# Box used to mount the VM, box examples: https://atlas.hashicorp.com/boxes/search
  config.vm.box = "ubuntu/trusty64"

# Version of box used
  config.vm.box_version = "20170602.0.0"

# Flag of box updates on all vagrant up. True by default
  config.vm.box_check_update = false

# Machine name. Null by default
  config.vm.hostname = "PICVM"

# Message to display after the vagrant up
  config.vm.post_up_message = "VM named PICVM started !!"

# Network configuration
# Portainer
  config.vm.network "forwarded_port", guest: 9079, host: 9079
# Jenkins
  config.vm.network "forwarded_port", guest: 9080, host: 9080
# Sonar
  config.vm.network "forwarded_port", guest: 9081, host: 9081

# Configuring of Vagrant's providers
  config.vm.provider "virtualbox" do |vb|
    vb.name = "portable-pic-box"
    vb.memory = "2048"
  end

  config.vm.provision "initInstall", type: "shell", preserve_order: true, inline: $initInstallScript
  config.vm.provision "initDirTree", type: "shell", preserve_order: true, inline: $initDirTreeScript
  config.vm.provision "initDockerConfig", type: "file", preserve_order: true, source: "./docker-compose.yml", destination: "#{HOME_CI}/docker-compose.yml"
  config.vm.provision "launchDockerEnv", type: "shell", preserve_order: true, inline: $launchScript, run: "always"

end